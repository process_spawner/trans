using ReflectionPlay.Extensions;
using ReflectionPlay.Models;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ReflectionPlayTest
{
    public class UnitTest1
    {
        [Fact]
        public void ImplementsIEnumerable_ShouldBe_True()
        {
            // Arrange
            var list = new List<string>();

            // Act
            list.AddRange(new List<string> { "Foo", "Bar"});

            // Assert
            Assert.True(list.ImplementsIEnumerable());
        }

        [Fact]
        public void TranslatableAttribute_ShouldBe_One()
        {
            // Arrange
            var model = new BlogEntry();

            // Act
            model.Title = "Foo";
            var result = model.GetTranslatableStrings();

            // Assert
            Assert.True(result.Count() == 1);
            Assert.True(result.First().Name == "Title");
            Assert.True(result.First()?.GetValue(model, null)?.ToString() == "Foo");

            result.First().SetValue(model, "Bar");
            Assert.True(result.First()?.GetValue(model, null)?.ToString() == "Bar");

        }

        [Fact]
        public void GetKeysOfTranslatables_ShouldBe_Three()
        {
            // Arrange
            var model = new BlogEntry { Title = "Foo" };
            var comment = new Comment { Title = "Bar", Author = new Author { Country = "Uruk" } };
            model.Comments.Add(comment);
            var storage = new List<string>();

            // Act
            model.GetKeysOfTranslatables(storage);
            
            // Assert
            Assert.True(storage.Count == 3);
        }

        [Fact]
        public void ApplyTranslations_Should_Translate()
        {
            // Arrange
            var model = new BlogEntry { Title = "Foo" };
            var comment = new Comment { Title = "Bar", Author = new Author { Country = "USA" } };
            model.Comments.Add(comment);
            var dictioanry = new Dictionary<string, string> { { "USA", "Mexico" } };

            // Act
            model.ApplyTranslations(dictioanry);

            // Assert
            Assert.True(comment.Author.Country == "Mexico");
        }

        [Fact]
        public void ApplyTranslations_On_List()
        {
            // Arrange
            var model = new BlogEntry { Title = "Foo" };
            var list = new List<BlogEntry> { new BlogEntry { Title = "Foo" }, new BlogEntry { Title = "Foo" }, new BlogEntry { Title = "Foo" } };
            var storage = new List<string>();
            var dictioanry = new Dictionary<string, string> { { "Foo", "Foo Translated" } };

            // Act
            model.GetKeysOfTranslatables(storage);
            list.ApplyTranslations(dictioanry);
            // Assert
            Assert.True(storage.Count == 1);
            list.ForEach(b => Assert.True(b.Title == "Foo Translated"));
        }

        [Fact]
        public void GetKeysOfTranslatables_On_List_500_BlogEntries()
        {
            // Arrange
            var list = ObjectGenerator.Shared.GenerateBlogEntries(500);
            var storage = new List<string>();


            // Act
            list.GetKeysOfTranslatables(storage);
            // Assert
            Assert.True(storage.Count == 500);
        }

        [Fact]
        public void GetKeysOfTranslatables_On_List_500_BlogEntries_And_1000_Comments()
        {
            // Arrange
            var list = ObjectGenerator.Shared.GenerateBlogEntries(500).ToArray();
            var commentsList = ObjectGenerator.Shared.GenerateComments(1000);
            var storage = new List<string>();
            list[10].Comments = commentsList.Take(250).ToList();
            list[200].Comments = commentsList.Take(500).ToList();
            list[498].Comments = commentsList.Take(249).ToList();
            list[499].Comments = commentsList.Take(1).ToList();

            // Act
            list.GetKeysOfTranslatables(storage);
            // Assert
            Assert.True(storage.Count == 1500);
        }

        [Fact]
        public void GetKeysOfTranslatables_On_Empty_List()
        {
            // Arrange
            var list = new List<BlogEntry>();
            var storage = new List<string>();


            // Act
            list.GetKeysOfTranslatables(storage);
            // Assert
            Assert.True(storage.Count == 0);
        }

        [Fact]
        public void GetKeysOfTranslatables_On_List_500_BlogEntries_With_Author_And_Its_BlogEntries()
        {
            // Arrange
            var list = ObjectGenerator.Shared.GenerateBlogEntries(500).ToArray();
            var list2 = ObjectGenerator.Shared.GenerateBlogEntries(50);
            var author = new Author { BlogEntries = list2.ToList() };

            var commentsList = ObjectGenerator.Shared.GenerateComments(1000);
            var storage = new List<string>();
            list[10].Comments = commentsList.Take(250).ToList();
            list[200].Comments = commentsList.Take(500).ToList();
            list[498].Comments = commentsList.Take(249).ToList();
            list[499].Comments = commentsList.Take(1).ToList();
            list[200].Comments[0].Author = author;

            // Act
            list.GetKeysOfTranslatables(storage);
            // Assert
            Assert.True(storage.Count == 1550);
        }

        [Fact]
        public void GetKeysOfTranslatables_On_List_500_BlogEntries_With_Author_And_Its_BlogEntries_Comment_Translated()
        {
            // Arrange
            var list = ObjectGenerator.Shared.GenerateBlogEntries(500).ToArray();
            var list2 = ObjectGenerator.Shared.GenerateBlogEntries(50);
            var author = new Author { BlogEntries = list2.ToList() };

            var commentsList = ObjectGenerator.Shared.GenerateComments(1000);
            var storage = new List<string>();
            var dictionary = new Dictionary<string, string> { { "Comment Title", "Comment Title Translated" } };

            list[10].Comments = commentsList.Take(250).ToList();
            list[200].Comments = commentsList.Take(500).ToList();
            list[498].Comments = commentsList.Take(249).ToList();
            list[499].Comments = commentsList.Take(1).ToList();
            list[200].Comments[0].Author = author;

            // Act
            list.GetKeysOfTranslatables(storage);
            list.ApplyTranslations(dictionary);
            // Assert
            Assert.True(storage.Count == 1550);
            Assert.True(list[200].Comments[0].Title == "Comment Title Translated");
        }
    }
}