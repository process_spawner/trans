﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionPlay.Models
{
    public class ObjectGenerator
    {
        private static ObjectGenerator? instance = null;
        private ObjectGenerator()
        {

        }

        public static ObjectGenerator Shared
        {
            get
            {
                if (instance == null)
                {
                    instance = new ObjectGenerator();
                }
                return instance;
            }
        }

        public IEnumerable<BlogEntry> GenerateBlogEntries(int amount)
        {
            return Enumerable.Range(1, amount).ToList().Select(entry => new BlogEntry() { Title = "BlogEntry Title"});
        }

        public IEnumerable<Comment> GenerateComments(int amount)
        {
            return Enumerable.Range(1, amount).ToList().Select(entry => new Comment() { Title = "Comment Title" });
        }
    }
}
