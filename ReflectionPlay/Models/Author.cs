﻿using ReflectionPlay.Attributes;
using ReflectionPlay.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionPlay.Models
{
    public class Author : ITranslatable
    {
        public string Name { get; set; } = string.Empty;

        [TranslatableString]
        public string Country { get; set; } = string.Empty;

        public List<BlogEntry> BlogEntries { get; set; } = new List<BlogEntry>();
    }
}
