﻿using ReflectionPlay.Attributes;
using ReflectionPlay.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionPlay.Models
{
    public class Comment : ITranslatable
    {
        public Author? Author { get; set; } = null;

        [TranslatableString]
        public string Title { get; set; } = string.Empty;

        public string Body { get; set; } = string.Empty;
    }
}
