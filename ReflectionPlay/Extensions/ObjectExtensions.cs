﻿using ReflectionPlay.Attributes;
using ReflectionPlay.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionPlay.Extensions
{
    public static class ObjectExtensions
    {

        public static void ApplyTranslations(this object self, Dictionary<string, string> dictionary)
        {
            if (self.ImplementsIEnumerable())
            {
                (self as IEnumerable<object>)?.ToList().ForEach(o => o.ApplyTranslations(dictionary));
            }
            else
            {
                if (self.ImplementsITranslatable())
                {
                    var translatableStrings = self.GetTranslatableStrings();
                    translatableStrings.ToList().ForEach(t =>
                    {
                        var keyValue = t.GetValue(self, null)?.ToString() ?? String.Empty;
                        if (!string.IsNullOrEmpty(keyValue))
                        {
                            dictionary.TryGetValue(keyValue, out string? textValue);
                            t.SetValue(self, textValue ?? string.Empty);
                        }
                    });

                    var untranslatableProperties = self.GetNotTranslatableProperties();
                    untranslatableProperties.ToList().ForEach(p => p.GetValue(self)?.ApplyTranslations(dictionary));
                }
            }
        }

        public static void GetKeysOfTranslatables(this object self, List<string> storage)
        {
            if (self.ImplementsIEnumerable())
            {
                (self as IEnumerable<object>)?.ToList().ForEach(o => o.GetKeysOfTranslatables(storage));
            }
            else
            {
                if (self.ImplementsITranslatable())
                {
                    var translatableStrings = self.GetTranslatableStrings();
                    storage.AddRange(translatableStrings.Select(p => p.GetValue(self, null)?.ToString() ?? String.Empty).Where(v => !string.IsNullOrEmpty(v)));

                    var untranslatableProperties = self.GetNotTranslatableProperties();
                    untranslatableProperties.ToList().ForEach(p => p.GetValue(self)?.GetKeysOfTranslatables(storage));
                }
            }
        }

        public static bool ImplementsIEnumerable(this object self) => self.GetType().GetInterfaces()
            .Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>));

        public static bool ImplementsITranslatable(this object self) => self.GetType().GetInterfaces()
            .Any(i => i == typeof(ITranslatable));

        public static bool IsClass(this object self) => self.GetType().IsClass();

        public static IEnumerable<PropertyInfo> GetTranslatableStrings(this object self) => 
                self.GetType()
                .GetProperties()
                .Where(p => p.CanRead && p.CanWrite)
                .Where(p => p.PropertyType == typeof(string))
                .Where(p => Attribute.IsDefined(p, typeof(TranslatableStringAttribute)));

        public static IEnumerable<PropertyInfo> GetNotTranslatableProperties(this object self) =>
                self.GetType()
                .GetProperties()
                .Where(p => p.CanRead && p.CanWrite)
                .Where(p => !Attribute.IsDefined(p, typeof(TranslatableStringAttribute)));
    }
}
