﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionPlay.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class TranslatableStringAttribute : Attribute
    {
    }
}
